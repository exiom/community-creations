# Alliance Auth Community Creations

A directory of community sourced plugins, tools, resources, etc. for Alliance Auth

## Contents

- [Plugins](#plugins)
- [Services](#services)
- [Deployment and Administration](#deployment-and-administration)
- [Developer resources](#developer-resources)

## Plugins

- [Alliance Auth / allianceauth-bbs](https://gitlab.com/allianceauth/allianceauth-bbs) - A simple forum for Allianceauth
- [Basraah / allianceauth-idp](https://gitlab.com/basraah/allianceauth-idp) - SAML 2.0 Identity Provider for Alliance Auth
- [Basraah / standingsrequests](https://gitlab.com/basraah/standingsrequests) - Alliance Auth compatible standings tool module for requesting alt character standings and checking API key registration.
- [colcrunch / fittings](https://gitlab.com/colcrunch/fittings) - A plugin for managing fittings and doctrines.
- [colcrunch / aa-moonstuff](https://gitlab.com/colcrunch/aa-moonstuff) - A plugin for publishing moon extractions, and keeping track of moon scan data.
- [Erik Kalkoken / aa-freight](https://gitlab.com/ErikKalkoken/aa-freight.git) - A plugin for running a central freight service for an alliance.
- [Erik Kalkoken / aa-dashboard-alerts](https://gitlab.com/ErikKalkoken/aa-dashboard-alerts) - Adds an alert message to the dashboard to notify users about their current open items like pending group requests and unread notifications.
- [Erik Kalkoken / aa-killtracker](https://gitlab.com/ErikKalkoken/aa-killtracker) - An app for running killmail trackers with Alliance Auth and Discord
- [Erik Kalkoken / aa-standingssync](https://gitlab.com/ErikKalkoken/aa-standingssync.git) - Enables non-alliance characters like scout alts to have the same standings view in game as their alliance main
- [Erik Kalkoken / aa-structures](https://gitlab.com/ErikKalkoken/aa-structures) - A plugin for monitoring alliance structures incl. a structure browser and structure notifications on Discord.
- [AaronKable / aa-corpstats-two](https://github.com/pvyParts/allianceauth-corpstats-two) - Extended Corpstats module for AllianceAuth with speed and functionality in mind.
- [AaronKable / allianceauth-signal-pings](https://github.com/pvyParts/allianceauth-signal-pings) - A simple plugin to send a "signal" to a discord webhook when a user does something in auth.
- [ppfeufer / aa-timezones](https://github.com/ppfeufer/aa-timezones) - Displaying different time zones within Alliance Auth.
- [ppfeufer / aa-discord-ping-formatter](https://github.com/ppfeufer/aa-discord-ping-formatter) - App for formatting fleet pings for Discord in Alliance Auth.
- [mckernanin / aa-graphql](https://github.com/mckernanin/aa-graphql) - GraphQL API For Alliance Auth

## Services

- [Alliance Auth / mumble-authenticator](https://gitlab.com/allianceauth/mumble-authenticator) - Authenticator script for the Alliance Auth Mumble integration
- [AaronKable / allianceauth-mumbletemps](https://github.com/pvyParts/allianceauth-mumble-temp) - Enables Templink access to Mumble for non authed characters
- [AaronKable / allianceauth-wiki-js](https://github.com/pvyParts/allianceauth-wiki-js) - Allianceauth service module for Wiki.js 

## Deployment and Administration

- [AaronKable / AllianceAuth-Celery-Analytics](https://github.com/pvyParts/allianceauth-celeryanalytics) - Celery task output logging to database for easy viewing and monitoring
- [MillerUK / AllianceAuth-Docker](https://github.com/milleruk/alliance_auth_docker) -  Docker-Compose Stack - Including Traefik Proxy
- [MillerUK/ AllianceAuth-Docker-Image](https://hub.docker.com/r/milleruk/allianceauth) - Docker image for AllianceAuth
- [mckernanin / Alliance Auth Kubernetes](https://github.com/mckernanin/alliance-auth-kubernetes) - Tutorial on setting up AA on a kubernetes cluster

## Developer resources

- [Erik Kalkoken / allianceauth-example-plugin](https://gitlab.com/ErikKalkoken/allianceauth-example-plugin) - Example plugin app that can be used as starting point / template for developing new plugin apps
- [Erik Kalkoken / django-eveuniverse](https://gitlab.com/ErikKalkoken/django-eveuniverse) - Complete set of Eve Online Universe models for Django with on-demand loading from ESI
- [Erik Kalkoken / a-dev-setup-wsl-vsc-v2.md](https://gist.github.com/ErikKalkoken/6fbcc8f27a0840836a811760d8c47216) - Guide on how to setup an Alliance Auth development environment on Windows 10 with WSL and Visual Studio Code
